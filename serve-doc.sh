#!/usr/bin/env bash
export APP_NAME=book-bff
cp ./doc/$APP_NAME.html ./doc/index.html
cd doc
ruby -run -e httpd . -p 8880
