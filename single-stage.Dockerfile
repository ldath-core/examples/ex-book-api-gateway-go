FROM alpine:latest

RUN mkdir -p "/var/application"
COPY public /var/application/public
COPY book-bff /bin/book-bff

EXPOSE 8080

ENTRYPOINT ["/bin/book-bff"]

CMD ["serve", "--config", "/secrets/local.env.yaml", "-b", "0.0.0.0", "-p", "8080", "-c"]
