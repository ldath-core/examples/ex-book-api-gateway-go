
function get_build_flags()
{
    profile_flag=""

    for var in "$@"
    do
        if [[ $var -eq "coverage" ]]; then
            profile_flag='-cover'
        fi
    done
    echo $profile_flag
}

function get_build_profile()
{
    # define simply build prefix
    build_prefix="Building project in "
    # define release mode as default one
    profile_name="release mode"
    # define empty OS
    os=""
    for var in "$@"
    do
        if [[ $var == "coverage" ]]; then
            profile_name="coverage mode"
        elif [[ $var == "linux" ]]; then
            os=" - Linux"
        fi
    done
    echo $build_prefix $profile_name $os
}

function get_os_architecture()
{
    for var in "$@"
    do
        if [[ $var == "linux" ]]; then
            # return
            # GOOS=linux
            # GOARCH=amd64
            echo "Configuration:"
            echo " -- GOOS=Linux"
            echo " -- GOARCH=`go env GOARCH`"
            GOOS=linux GOARCH=`go env GOARCH`
        fi
    done
}