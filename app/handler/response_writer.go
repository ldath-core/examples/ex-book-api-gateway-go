package handler

import (
	"encoding/json"
	"net/http"

	"gitlab.com/mobica-workshops/examples/go/gorilla/book-bff/app/model"
)

// ResponseWriter will write result in http.ResponseWriter
func ResponseWriter(w http.ResponseWriter, statusCode int, message string, data interface{}) error {
	w.Header().Set("Content-Type", "application/json")
	httpResponse := model.NewResponse(statusCode, message, data)
	err := json.NewEncoder(w).Encode(httpResponse)
	return err
}

// ErrorResponseWriter will write result in http.ResponseWriter
func ErrorResponseWriter(res http.ResponseWriter, statusCode int, message string, errors []string) error {
	res.WriteHeader(statusCode)
	httpResponse := model.NewErrorResponse(statusCode, message, errors)
	err := json.NewEncoder(res).Encode(httpResponse)
	return err
}

func RespondWithError(w http.ResponseWriter, statusCode int, error model.Error) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(error)
}

func ResponseJSON(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(data)
}
