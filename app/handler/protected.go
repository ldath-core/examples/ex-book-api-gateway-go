package handler

import (
	"net/http"

	"gitlab.com/mobica-workshops/examples/go/gorilla/book-bff/app/model"
)

func (c Controller) ProtectedEndpoint(w http.ResponseWriter, r *http.Request) {
	ResponseJSON(w, model.Response{Status: 200, Message: "Protected"})
}
