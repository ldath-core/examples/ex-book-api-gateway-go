package handler

import (
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/mobica-workshops/examples/go/gorilla/book-bff/app/lib"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-bff/app/model"
	"golang.org/x/crypto/bcrypt"
)

type GetAdminsResponse struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Content model.PaginatedBookAdminsContent
}

func (c Controller) Login(w http.ResponseWriter, r *http.Request) {
	var user model.User
	var jwt model.JWT
	var errModel model.Error

	json.NewDecoder(r.Body).Decode(&user)

	errModel.Message = "Invalid credentials"
	if user.Email == "" {
		errModel.Errors = append(errModel.Errors, "Empty Email is not allowed")
	}
	if user.Password == "" {
		errModel.Errors = append(errModel.Errors, "Empty Password is not allowed")
	}
	if len(errModel.Errors) > 0 {
		RespondWithError(w, http.StatusBadRequest, errModel)
		return
	}

	bookAdminOutput, err := getAdminsByEmail(c.Config.API.BookAdmin.Url, user.Email)
	if err != nil {
		errModel.Message = "Internal Server Error"
		RespondWithError(w, http.StatusInternalServerError, errModel)
		return
	}
	if bookAdminOutput.Content.Count > 1 {
		errModel.Message = "Internal Server Error"
		c.Logger.Error("Broken Database")
		RespondWithError(w, http.StatusInternalServerError, errModel)
		return
	} else if bookAdminOutput.Content.Count < 1 {
		errModel.Message = "Failed to authenticate. Check your login data."
		c.Logger.Debug("User Not Found")
		RespondWithError(w, http.StatusBadRequest, errModel)
		return
	}

	hashedPassword := bookAdminOutput.Content.Results[0].PasswordHash

	if checkPasswordHash(user.Password, hashedPassword) {
		token, err := lib.GenerateToken(user, c.Config)
		if err != nil {
			errModel.Message = "Failed to authenticate. Check your login data."
			c.Logger.Debug("Broken Token")
			RespondWithError(w, http.StatusBadRequest, errModel)
			return
		}
		jwt.Token = token
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Authorization", token)
		ResponseWriter(w, http.StatusOK, "JWT - token", jwt)
	} else {
		errModel.Message = "Failed to authenticate. Check your login data."
		errModel.Errors = append(errModel.Errors, "Invalid Password")
		c.Logger.Debug("Wrong Password")
		RespondWithError(w, http.StatusBadRequest, errModel)
		return
	}
}

func getAdminsByEmail(apiUrl string, email string) (GetAdminsResponse, error) {
	qMap := map[string]string{"skip": "0", "limit": "2", "email": email}

	urlPath := formatPath(apiUrl, "v1/admins")

	fullUrl, err := getApiUrlWithPath(urlPath, qMap)
	if err != nil {
		return GetAdminsResponse{}, err
	}

	_, body, _ := getApiBody(fullUrl, time.Duration(2), http.MethodGet)

	result := GetAdminsResponse{}
	jsonErr := json.Unmarshal(body, &result)
	if jsonErr != nil {
		return GetAdminsResponse{}, jsonErr
	}
	return result, nil
}

func checkPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
