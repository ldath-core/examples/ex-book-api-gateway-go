package handler

import (
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-bff/config"
)

const succeed = "\u2713"
const failed = "\u2717"

type CreateTestData struct {
	DropCollection bool
	LoadTestData   bool
}

func createNewRecorder(r *mux.Router, method, endpoint string, qData map[string]string, body io.Reader) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, endpoint, body)
	if len(qData) >= 0 {
		q := req.URL.Query()
		for k, v := range qData {
			q.Add(k, v)
		}
		req.URL.RawQuery = q.Encode()
	}

	rr := httptest.NewRecorder()
	r.ServeHTTP(rr, req)
	return rr
}

func setup(t *testing.T, ctx context.Context, createTestData CreateTestData) Controller {
	l, r, c := config.GetTestSetup(t, ctx)
	controller := Controller{
		RDB:    r,
		Logger: l,
		Config: c,
	}

	return controller
}

func testResponseStatusCheck(t *testing.T, expectedStatusCode int, rr *httptest.ResponseRecorder) {
	if status := rr.Code; status != expectedStatusCode {
		t.Errorf("%s check StatusCreated is failed: got %d want %d", failed, status, expectedStatusCode)
		t.Errorf("BODY: %s", rr.Body)
	} else {
		t.Logf("%s check status %d is successfull.", succeed, expectedStatusCode)
	}
}
