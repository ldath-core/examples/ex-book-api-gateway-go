package handler

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"testing"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-bff/app/model"
)

func TestLoginFailWithEmptyPasswordAndEmail(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{})

	e, _ := json.Marshal(model.User{
		Password:          "",
		Email:             "",
		ReturnSecureToken: false,
	})

	r := mux.NewRouter()
	r.HandleFunc("/v1/login", c.Login)

	rr := createNewRecorder(r, "POST", "/v1/login", nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusBadRequest, rr)

	var errModel model.Error
	json.NewDecoder(rr.Result().Body).Decode(&errModel)

	if len(errModel.Errors) < 2 {
		t.Errorf("Error table is empty")
		t.Errorf("BODY: %s", rr.Body)
	} else {
		t.Logf("Error Array is filled with email and password error logs")
	}
}

func TestLoginFailWithEmptyPassword(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{})

	e, _ := json.Marshal(model.User{
		Password:          "",
		Email:             "test123@gmail.com",
		ReturnSecureToken: false,
	})

	r := mux.NewRouter()
	r.HandleFunc("/v1/login", c.Login)

	rr := createNewRecorder(r, "POST", "/v1/login", nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusBadRequest, rr)

	var errModel model.Error
	json.NewDecoder(rr.Result().Body).Decode(&errModel)

	if len(errModel.Errors) < 1 {
		t.Errorf("Error table is empty")
		t.Errorf("BODY: %s", rr.Body)
	} else {
		t.Logf("Error Array is filled with password log")
	}

	if errModel.Errors[0] != "Empty Password is not allowed" {
		t.Errorf("Error table is empty")
		t.Errorf("BODY: %s", rr.Body)
	} else {
		t.Logf("Error Array is filled with password error log")
	}
}

func TestLoginFailWithEmptyEmail(t *testing.T) {
	// CTX
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	c := setup(t, ctx, CreateTestData{})

	e, _ := json.Marshal(model.User{
		Password:          "Test123",
		Email:             "",
		ReturnSecureToken: false,
	})

	r := mux.NewRouter()
	r.HandleFunc("/v1/login", c.Login)

	rr := createNewRecorder(r, "POST", "/v1/login", nil, bytes.NewBuffer(e))
	testResponseStatusCheck(t, http.StatusBadRequest, rr)

	var errModel model.Error
	json.NewDecoder(rr.Result().Body).Decode(&errModel)

	if len(errModel.Errors) < 1 {
		t.Errorf("Error table is empty")
		t.Errorf("BODY: %s", rr.Body)
	} else {
		t.Logf("Error Array is filled with email log")
	}

	if errModel.Errors[0] != "Empty Email is not allowed" {
		t.Errorf("Error table is empty")
		t.Errorf("BODY: %s", rr.Body)
	} else {
		t.Logf("Error Array is filled with email error log")
	}
}
