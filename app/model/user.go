package model

type User struct {
	Email             string `json:"email"`
	Password          string `json:"password"`
	ReturnSecureToken bool   `json:"returnSecureToken"`
}

type BookAdminObject struct {
	ID           string `json:"id"`
	Email        string `json:"email"`
	FirstName    string `json:"firstName"`
	LastName     string `json:"lastName"`
	PasswordHash string `json:"passwordHash"`
	Version      int    `json:"version"`
}

type BookAdminResults []BookAdminObject

type PaginatedBookAdminsContent struct {
	Count   int64 `json:"count"`
	Skip    int64 `json:"skip"`
	Limit   int64 `json:"limit"`
	Results BookAdminResults
}
