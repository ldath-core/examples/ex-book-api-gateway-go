package model

type RequestDesc struct {
	RequestType string
	UriPath     string
}
