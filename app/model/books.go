package model

// Book is the data structure that we will save and receive.
type Book struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Author      string `json:"author"`
	Year        string `json:"year"`
	Description string `json:"description"`
	Img         Image  `json:"image,omitempty"`
}

type Image struct {
	Base64 string `json:"base64,omitempty"`
	Url    string `json:"url,omitempty"`
}

type CreatedBook struct {
	ID int `json:"id"`
}
