package lib

import (
	"time"

	"github.com/dgrijalva/jwt-go"

	"gitlab.com/mobica-workshops/examples/go/gorilla/book-bff/app/model"
	"gitlab.com/mobica-workshops/examples/go/gorilla/book-bff/config"
	"golang.org/x/crypto/bcrypt"
)

const (
	expireAtDefault = "12h00m00s"
)

func ComparePasswords(hashedPassword string, password []byte) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))

	return err == nil
}

func GenerateToken(user model.User, c *config.Config) (string, error) {
	expTime, err := setExpirationTime(c.API.TokenExpTime)
	if err != nil {
		return "", err
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email": user.Email,
		"iss":   "course",
		"exp":   time.Now().Add(expTime).Unix(),
	})

	tokenString, err := token.SignedString([]byte(c.Server.JwtSecret))

	return tokenString, err
}

func setExpirationTime(expireAt string) (time.Duration, error) {
	if expireAt == "" {
		return time.ParseDuration(expireAtDefault)
	} else {
		return time.ParseDuration(expireAt)
	}
}
