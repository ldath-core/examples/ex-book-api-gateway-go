#!/usr/bin/env bash
set -e
kubectx k3d-bookCluster
./build-multi-stage-container.sh
k3d image import -c bookCluster book-bff:latest
helm upgrade -i -f secrets/k3s-values.yaml --namespace=services k3s-book-bff ex-book/book-service --version 0.6.1 --dry-run --debug
helm upgrade -i -f secrets/k3s-values.yaml --namespace=services k3s-book-bff ex-book/book-service --version 0.6.1 --wait
