package config

import (
	"context"
	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"os"
	"testing"
	"time"
)

func getTestConfig() (*Config, error) {
	cfgFile := os.Getenv("CFG_FILE")
	viper.SetConfigFile(cfgFile)
	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}
	return LoadConfig("text")
}

func GetTestSetup(t *testing.T, ctx context.Context) (l *logrus.Logger, r *redis.Client, c *Config) {
	// Logger
	l = logrus.New()

	// Config
	c, e := getTestConfig()
	if e != nil {
		t.Fatalf("Error: %s", e.Error())
	}

	// REDIS
	r = redis.NewClient(&redis.Options{
		Addr:         c.Redis.Uri,
		DialTimeout:  10 * time.Second,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		PoolSize:     10,
		PoolTimeout:  30 * time.Second,
		Password:     "", // no password set
		DB:           0,  // use default DB
	})

	return l, r, c
}
