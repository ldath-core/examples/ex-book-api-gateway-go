package config

import (
	"errors"
	"os"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type LoggerConfig struct {
	Level string `mapstructure:"level"`
}

type SentryConfig struct {
	Dsn string `mapstructure:"dsn"`
}

type RedisConfig struct {
	Uri string `mapstructure:"uri"`
}

type ServerConfig struct {
	Host      string `mapstructure:"host"`
	Port      string `mapstructure:"port"`
	JwtSecret string `mapstructure:"jwt-secret"`
}

type BookListConfig struct {
	Url string `mapstructure:"url"`
}

type BookAdminConfig struct {
	Url string `mapstructure:"url"`
}

type ApiConfig struct {
	BookList     BookListConfig  `mapstructure:"book-list"`
	BookAdmin    BookAdminConfig `mapstructure:"book-admin"`
	TokenExpTime string          `mapstructure:"token-expiration"`
}

type Config struct {
	Environment string       `mapstructure:"env"`
	Server      ServerConfig `mapstructure:"server"`
	Logger      LoggerConfig `mapstructure:"logger"`
	Sentry      SentryConfig `mapstructure:"sentry"`
	Redis       RedisConfig  `mapstructure:"redis"`
	API         ApiConfig    `mapstructure:"api"`
}

func LoadConfig(format string) (*Config, error) {
	var config *Config

	logrus.SetOutput(os.Stdout)
	if strings.ToLower(format) == "json" {
		logrus.SetFormatter(&logrus.JSONFormatter{})
		//logrus.SetReportCaller(true)
	} else {
		logrus.SetFormatter(&logrus.TextFormatter{
			DisableColors: false,
			FullTimestamp: true,
		})
	}

	if err := viper.Unmarshal(&config); err != nil {
		logrus.WithFields(
			logrus.Fields{
				"err": err,
			},
		).Errorln("Broken config file")
		return config, err
	}
	if config.Logger.Level == "" {
		logrus.Errorln("Missing config file")
		time.Sleep(5 * time.Minute)
		return config, errors.New("missing config file")
	}
	return config, nil
}
